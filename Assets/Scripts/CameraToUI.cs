using Mirror;
using UnityEngine;
using UnityEngine.UI;

public class CameraToUI : MonoBehaviour
{
    [SerializeField] private Camera secondaryCamera;
    [SerializeField] private RawImage rawImage;

    void Start()
    {
        if (NetworkServer.active)
        {
            rawImage.gameObject.SetActive(false);
            secondaryCamera.gameObject.SetActive(false);   
            return;
        }
        
        ColorSelectionPanel.Instance.OnColorSelectionPanelChangeState += (state) =>
        {
            rawImage.gameObject.SetActive(state);
        };
        
        if (secondaryCamera.targetTexture != null)
        {
            secondaryCamera.targetTexture.Release();
        }
        
        secondaryCamera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        rawImage.texture = secondaryCamera.targetTexture;
    }
}