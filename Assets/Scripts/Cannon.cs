using Mirror;
using UnityEngine;

public class Cannon : NetworkBehaviour
{
    [SerializeField] private Transform rotater;
    [SerializeField] private Transform cannon;
    [SerializeField] private Transform cameraPoint;
    [SerializeField] private GameObject firePoint;
    [SerializeField] private GameObject wallPoint;
    [SerializeField] [SyncVar] private float shootForceMax;
    [SerializeField] [SyncVar] private float maxSecHold;
    [SerializeField] [SyncVar] private float secHold;
    private Camera mainCamera;
    [SerializeField] private GameObject cannonObj;
    [SyncVar(hook = nameof(SyncColor))] [SerializeField]
    private Color color;
    
    public float rotationSpeed = 5f;
    public float elevationSpeed = 5f;
    public float minElevationAngle = -20f;
    public float maxElevationAngle = 60f;
    
    void Start()
    {
        if (isOwned)
        {
            mainCamera = Camera.main;
            mainCamera.transform.SetParent(cameraPoint);
            mainCamera.transform.localPosition = Vector3.zero;
            mainCamera.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }

    void Update()
    {
        
        if (isOwned)
        {
            var view = mainCamera.ScreenToViewportPoint(Input.mousePosition);
            var isOutside = view.x < 0 || view.x > 1 || view.y < 0 || view.y > 1;
            
            if (!isOutside)
            {
                CalculateRotation();
                CalculateFire();
            }
        }
    }

    private void CalculateFire()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CmdFirePressed();
        }
        else if (Input.GetMouseButton(0))
        {
            CmdFireHold();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            CmdFireReleased();
        }
    }

    [Command]
    private void CmdFirePressed()
    {
        secHold = 0;
    }

    [Command]
    private void CmdFireHold()
    {
        secHold += Time.deltaTime;
    }

    [Command]
    private void CmdFireReleased()
    {
        if (secHold > 0)
        {
            GameObject newProjectile = Spawner.Instance.SpawnProjectile(firePoint.transform.position);
            //GameObject newProjectile = Instantiate(projectile, firePoint.transform.position, Quaternion.identity);
            //NetworkServer.Spawn(newProjectile);
            Vector3 vectorToShoot = firePoint.transform.position - cannonObj.transform.position;
            float shootForcePercent =Mathf.Clamp(secHold / maxSecHold,0,1) ;
            newProjectile.GetComponent<Rigidbody>().AddForce(vectorToShoot.normalized * shootForceMax * shootForcePercent);
            int cannonPlayerId = GetComponent<NetworkIdentity>().connectionToClient.connectionId;
            newProjectile.GetComponent<Projectile>().SetPlayerId(cannonPlayerId);
        }
    }
    

    private void CalculateRotation()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            CmdRotateTower(hit.point);
        }
    }

    [Command]
    private void CmdRotateTower(Vector3 hit)
    {
        Vector3 targetDirectionRotater = hit - rotater.position;
        targetDirectionRotater.y = 0; 

     
        Quaternion rotaterRotation = Quaternion.LookRotation(targetDirectionRotater);
        Quaternion targetRotaterRotation = Quaternion.LookRotation(targetDirectionRotater);
        if (Quaternion.Angle(rotater.rotation, targetRotaterRotation) > 0.1f)
        {
            rotater.rotation = Quaternion.Slerp(rotater.rotation, targetRotaterRotation, Time.deltaTime * rotationSpeed);
        }
        rotater.rotation = Quaternion.Slerp(rotater.rotation, rotaterRotation, Time.deltaTime * rotationSpeed);

       
        Vector3 targetDirectionCannon = hit - cannon.position;
        
        Quaternion cannonRotation = Quaternion.LookRotation(targetDirectionCannon);
      
        cannonRotation.x = Mathf.Clamp(cannonRotation.x, minElevationAngle, maxElevationAngle);

     
        cannon.localRotation = Quaternion.Euler(cannonRotation.eulerAngles.x, 0f, 0f);
        RpcRotateTower(rotater.rotation,cannon.localRotation);
    }

    [ClientRpc]
    private void RpcRotateTower(Quaternion rotaterRotation, Quaternion cannonLocalRotation)
    {
        rotater.rotation = rotaterRotation;
        cannon.localRotation = cannonLocalRotation;
    }
    
    public void SetCannonColor(Color color)
    {
        this.color = color;
    }

    private void SyncColor(Color oldVal, Color newVal)
    {
        cannonObj.GetComponent<Renderer>().material.color = newVal;
    }

    public Transform GetWallPoint()
    {
        return wallPoint.transform;
    }
    
}