using Mirror;
using UnityEngine;

public class MyInGamePlayer : NetworkBehaviour
{
    public Color Color => color;
    public string Name => name;

    [SerializeField] public PlayerWorldCanvas PlayerCanvas;
    public int Score => score;

    [SyncVar] private int score;
    [SyncVar] [SerializeField] private Color color;
    
    private string name;

    
    public void ChangePlayerScore(int newScore)
    {
        score = newScore;
        
        if (PlayerCanvas)
        {
            PlayerCanvas.Score = newScore;
        }
        
    }
    public void ChangePlayerColor(Color newColor)
    {
        color = newColor;
    }

    public void ChangePlayerName(string newName)
    {
        name = newName;
        if (PlayerCanvas)
        {
            PlayerCanvas.Name = newName;
        }
    }
}