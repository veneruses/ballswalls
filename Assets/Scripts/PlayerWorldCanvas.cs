using Mirror;
using TMPro;
using UnityEngine;

public class PlayerWorldCanvas : NetworkBehaviour
{
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            scoresUI.text = new string($"Scores:{value}");
        }
    }

    public string Name
    {
        get { return name; }
        set
        {
            name = value;
            nameUI.text = value;
        }
    }
    
    [SerializeField] private TextMeshProUGUI nameUI;
    [SerializeField] private TextMeshProUGUI scoresUI;
    [SyncVar(hook = nameof(SyncScores))] [SerializeField] private int score;
    [SyncVar(hook = nameof(SyncName))] [SerializeField] private string name;
    
    private void SyncName(string oldval, string newVal)
    {
        nameUI.text = newVal;
    }

    private void SyncScores(int oldVal, int newVal)
    {
        scoresUI.text = new string($"Scores:{newVal}");
    }
}