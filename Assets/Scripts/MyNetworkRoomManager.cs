using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class MyNetworkRoomManager : NetworkRoomManager
{
    public static MyNetworkRoomManager Instance
    {
        get { return instance; }
    }

    public Action<NetworkConnectionToClient> OnPlayerDisconnect;
    public Action<NetworkConnectionToClient> OnPlayerConnect;
    public Action<NetworkBehaviour> OnPlayerSpawned;
    public Action OnServerStarted;

    private Dictionary<int, PlayerData> playerDatas = new Dictionary<int, PlayerData>();

    private static MyNetworkRoomManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        clientIndex++;

        if (Utils.IsSceneActive(RoomScene))
        {
            allPlayersReady = false;
            GameObject spawnedPlayer = MyLobby.Instance.SpawnPlayer(roomPlayerPrefab.gameObject, conn);
            GameManager.Instance.SetLocalPlayer(conn, spawnedPlayer.GetComponent<NetworkBehaviour>());
        }
        else
        {
            GameObject player = Instantiate(playerPrefab.gameObject, Vector3.zero, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player);
            MyInGamePlayer inGamePlayer =
                NetworkServer.connections[conn.connectionId].identity.GetComponent<MyInGamePlayer>();
            inGamePlayer.ChangePlayerColor(playerDatas[conn.connectionId].Color);
            inGamePlayer.ChangePlayerName(playerDatas[conn.connectionId].Name);
            GameManager.Instance.SetLocalPlayer(conn, player.GetComponent<NetworkBehaviour>());

            Spawner.Instance.SpawnPlayerCannon(conn);
        }
    }


    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        base.OnServerDisconnect(conn);
        OnPlayerDisconnect?.Invoke(conn);
    }

    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        base.OnServerConnect(conn);
        OnPlayerConnect?.Invoke(conn);
    }
    public void LoadGameScene()
    {
        List<MyNetworkRoomPlayer> players = new List<MyNetworkRoomPlayer>();


        foreach (var conn in NetworkServer.connections)
        {
            if (conn.Value != null && conn.Value.identity != null)
            {
                MyNetworkRoomPlayer player = conn.Value.identity?.GetComponent<MyNetworkRoomPlayer>();
                if (player != null)
                {
                    players.Add(player);
                }
            }
        }

        foreach (var player in players)
        {
            playerDatas.Add(player.connectionToClient.connectionId, new PlayerData()
            {
                Color = player.PlayerColor,
                Name = player.PlayerName
            });
        }

        ServerChangeScene(GameplayScene);
    }
}