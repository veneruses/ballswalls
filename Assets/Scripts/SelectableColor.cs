using UnityEngine;
using UnityEngine.UI;

public class SelectableColor : MonoBehaviour
{
    public Color Color => color;
    
    [SerializeField] private Color color;
    [SerializeField] private ColorSelectionPanel colorSelectionPanel;
    private bool isAvailable;
    

    public void Init(Color color, ColorSelectionPanel selectionPanel)
    {
        this.color = color;
        this.colorSelectionPanel = selectionPanel;
        isAvailable = true;

    }

    public void TakeColor()
    {
        colorSelectionPanel.TakeColor(color);
    }
    
    public void SetColorAvailable(bool state)
    {
        GetComponent<Button>().interactable = state;
    }
}