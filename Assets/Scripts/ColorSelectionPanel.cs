using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ColorSelectionPanel : NetworkBehaviour
{
    private static ColorSelectionPanel instance;

    public static ColorSelectionPanel Instance
    {
        get { return instance; }
    }
    [FormerlySerializedAs("playerColors")] [SerializeField]
    private List<Color> playerColorsAll;

    private Dictionary<NetworkConnectionToClient, Color> playersColors;

    [SerializeField] private MyNetworkRoomPlayer playerUI;
    [SerializeField] private List<Color> playerColorsAvailable;
    [SerializeField] private GridLayoutGroup colorGrid;
    [SerializeField] private Vector2 colorImgSize = new Vector2(150, 150);

    [SerializeField] private Button activateColorPanelButton;
    [SerializeField] private Button deactivateColorPanelButton;
    public Action<Color> OnColorChanged;
    public Action<bool> OnColorSelectionPanelChangeState;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
    }

    public override void OnStartServer()
    {
        if (isServer)
        {
            playerColorsAvailable = new List<Color>(playerColorsAll);
            playersColors = new Dictionary<NetworkConnectionToClient, Color>();
            MyNetworkRoomManager.Instance.OnPlayerDisconnect += (client =>
            {
                if (playersColors.ContainsKey(client))
                {
                    playerColorsAvailable.Add(playersColors[client]);
                    playersColors.Remove(client);
                }
            });
            
            MyNetworkRoomManager.Instance.OnPlayerConnect += (client =>
            {
                if (playersColors.ContainsKey(client))
                {
                    playerColorsAvailable.Add(playersColors[client]);
                    playersColors.Remove(client);
                }
            });
        }
        
    }


    public override void OnStartClient()
    {
        activateColorPanelButton.onClick.AddListener(() =>
        {
            bool gridActive = !colorGrid.gameObject.activeSelf;
            colorGrid.gameObject.SetActive(gridActive);
            OnColorSelectionPanelChangeState?.Invoke(gridActive);
        });
        
        deactivateColorPanelButton.onClick.AddListener(() =>
        {
            colorGrid.gameObject.SetActive(false);
            OnColorSelectionPanelChangeState?.Invoke(false);
        });
        
        colorGrid.cellSize = colorImgSize;
        InstantiateColors();
        CmdRefreshColorState();
    }

    private void InstantiateColors()
    {
        if (playerColorsAll == null && playerColorsAll.Count < 1) return;

        for (int i = 0; i < playerColorsAll.Count; i++)
        {
            GameObject newColorObj = new GameObject();
            newColorObj.AddComponent<Image>().color = playerColorsAll[i];
            newColorObj.AddComponent<Button>();
            SelectableColor selColor = newColorObj.AddComponent<SelectableColor>();
            selColor.Init(playerColorsAll[i], this);
            selColor.GetComponent<Button>().onClick.AddListener(selColor.TakeColor);
            newColorObj.transform.SetParent(colorGrid.transform,false);
            LayoutRebuilder.ForceRebuildLayoutImmediate(colorGrid.GetComponent<RectTransform>());
        }
    }

    public void TakeColor(Color color)
    {
        if (playerUI.PlayerColor != color)
        {
            if (playerUI.PlayerColor != Color.clear)
            {
                CmdTurnColorBack(playerUI.PlayerColor);
            }

            CmdTakeColor(playerUI.gameObject, color);
        }
    }

    [Command(requiresAuthority = false)]
    public void CmdTakeColor(GameObject player, Color color)
    {
        if (!playerColorsAvailable.Contains(color)) return;
        
        playerColorsAvailable.Remove(color);
        NetworkIdentity playerIdentity = player.GetComponent<NetworkIdentity>();
        if (playersColors.ContainsKey(playerIdentity.connectionToClient))
        {
            playersColors[playerIdentity.connectionToClient] = color;
        }
        else
        {
            playersColors.Add(playerIdentity.connectionToClient, color);
        }

        TargetTakeColor(playerIdentity.connectionToClient, color);
        RpcRefreshColorState(playerColorsAvailable);
    }

    [TargetRpc]
    public void TargetTakeColor(NetworkConnectionToClient conn, Color color)
    {
        playerUI.CmdSetColor(color);
        FindObjectOfType<LobbyCannon>().SetCannonColor(color);
        OnColorChanged?.Invoke(color);
    }

    [ClientRpc]
    private void RpcRefreshColorState(List<Color> availableColors)
    {
        foreach (var selectableColor in colorGrid.GetComponentsInChildren<SelectableColor>())
        {
            selectableColor.SetColorAvailable(false);
            if (availableColors.Contains(selectableColor.Color))
            {
                selectableColor.SetColorAvailable(true);
            }
        }
    }
    
    [Command(requiresAuthority = false)]
    private void CmdRefreshColorState()
    {
        RpcRefreshColorState(playerColorsAvailable);
    }
    

    [Command(requiresAuthority = false)]
    public void CmdTurnColorBack(Color color)
    {
        if(playerColorsAvailable.Contains(color)) return;
        playerColorsAvailable.Add(color);
        RpcRefreshColorState(playerColorsAvailable);
    }

    public void SetPlayerObject(MyNetworkRoomPlayer player)
    {
        playerUI = player;
    }

    public override void OnStopClient()
    {
        base.OnStopClient();
    }
}