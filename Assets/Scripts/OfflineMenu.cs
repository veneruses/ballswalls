using System;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OfflineMenu : MonoBehaviour
{

    public static OfflineMenu Instance
    {
        get { return instance; }
    }


    [SerializeField] private TMP_InputField nameInput;
    [SerializeField] private TMP_InputField ipInput;
    [SerializeField] private Button connectButton;
    private static OfflineMenu instance;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (this != instance)
            {
                Destroy(gameObject);
            }
        }
    }

    private void Start()
    {
        nameInput.onValueChanged.AddListener(SetPlayerName);
        ipInput.onValueChanged.AddListener(SetIP);
        connectButton.interactable = false;
        nameInput.onValueChanged.AddListener((name) => { connectButton.interactable = !String.IsNullOrEmpty(name); });
    }

    public void SetPlayerName(string name)
    {
        if (!String.IsNullOrEmpty(name))
        {
            PlayerPrefs.SetString("BallsWalls_PlayerName", name);
        }
    }
    
    public void SetIP(string ip)
    {
        if (!String.IsNullOrEmpty(ip))
        {
            NetworkManager.singleton.networkAddress = ip;
        }
    }
}