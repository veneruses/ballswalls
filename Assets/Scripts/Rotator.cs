using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private float speed;

    private void Update()
    {
        gameObject.transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * speed);
    }
}