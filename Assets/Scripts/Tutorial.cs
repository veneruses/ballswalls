using Mirror;
using UnityEngine;

public class Tutorial : NetworkBehaviour
{
    [SerializeField] private float timeToDisabe = 10;
    [SerializeField] private GameObject tutorialPanel;
    void Start()
    {
        if (isServer)
        {
            tutorialPanel.SetActive(false);
        }
    }

    private void Update()
    {
        if (isClient)
        {
            timeToDisabe -= Time.deltaTime;
            if (timeToDisabe<0)
            {
                tutorialPanel.SetActive(false);
            }
        }
    }
}
