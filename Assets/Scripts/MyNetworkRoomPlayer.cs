using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MyNetworkRoomPlayer : NetworkRoomPlayer
{
    public Color PlayerColor => playerColor;
    public string PlayerName => playerName;

    [SerializeField] private TextMeshProUGUI nameText;
    [SyncVar(hook = nameof(SyncColor))] [SerializeField] private Color playerColor;
    [SerializeField] private Image colorImg;
    [SyncVar(hook = nameof(SyncName))] private string playerName;
    [SyncVar(hook = nameof(SyncReady))] private bool isReady;
    [SerializeField] private Toggle readyToggle;


    public void Start()
    {
        gameObject.GetComponent<RectTransform>().SetParent(MyLobby.Instance.PlayerPrefabParent.transform, false);
        LayoutRebuilder.ForceRebuildLayoutImmediate(MyLobby.Instance.PlayerPrefabParent.GetComponent<RectTransform>());


        if (isOwned)
        {
            string newName = PlayerPrefs.GetString("BallsWalls_PlayerName", "Player");
            CmdSetName(newName);
            FindObjectOfType<ColorSelectionPanel>().SetPlayerObject(this);
        }
    }

    [Command]
    private void CmdSetName(string name)
    {
        playerName = name;
        nameText.text = name;
    }

    private void SyncName(string oldName, string newName)
    {
        nameText.text = newName;
    }

    public void SyncColor(Color oldColor, Color newColor)
    {
        colorImg.color = newColor;
    }

    public void SyncReady(bool oldVal, bool newVal)
    {
        readyToggle.isOn = newVal;
    }

    [Command]
    public void CmdSetColor(Color color)
    {
        playerColor = color;
    }

    [Server]
    public void ServerSetReady(bool state)
    {
        isReady = state;
    }
}