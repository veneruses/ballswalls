using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MyLobby : NetworkBehaviour
{
    public static MyLobby Instance
    {
        get { return instance; }
    }
    public GameObject PlayerPrefabParent => playerPrefabParent;

    private static MyLobby instance;
    [SerializeField] private TextMeshProUGUI startGameCounter;
    [SyncVar(hook = nameof(SyncCounter))] private string counterText;
    [SerializeField] private Button readyButton;
    [SerializeField] private GameObject playerPrefabParent;
    [SerializeField] private NetworkConnectionToClient localConnectionToClient;
    private Dictionary<NetworkConnectionToClient, bool> readyPlayers;
    private Coroutine startGameCounterCor;
    private bool stopStartCounter;

    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
        
        
        readyButton.onClick.AddListener(() =>
        {
            CmdReadyClicked(NetworkClient.connection.identity.connectionToClient);
        });
    }


    private void Start()
    {
        ColorSelectionPanel.Instance.OnColorChanged += color =>
        {
            readyButton.interactable = true;
        };
    }

    public override void OnStartServer()
    {
        if (isServer)
        {
            readyPlayers = new Dictionary<NetworkConnectionToClient, bool>();
            MyNetworkRoomManager.Instance.OnPlayerDisconnect += (connectionToClient) =>
            {
                if (readyPlayers.ContainsKey(connectionToClient))
                {
                    readyPlayers.Remove(connectionToClient);
                }
            };
            MyNetworkRoomManager.Instance.OnPlayerConnect += (connectionToClient) =>
            {
                if (!readyPlayers.ContainsKey(connectionToClient))
                {
                    readyPlayers.Add(connectionToClient, false);
                }
            };
        }
    }

    public GameObject SpawnPlayer(GameObject player, NetworkConnectionToClient conn)
    {
        GameObject roomPlayer = Instantiate(player, Vector3.zero, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, roomPlayer);
        return roomPlayer;
    }

    [Command(requiresAuthority = false)]
    private void CmdReadyClicked(NetworkConnectionToClient conn)
    {
        MyNetworkRoomPlayer playerCalledCmd =
            NetworkServer.connections[conn.connectionId]?.identity.GetComponent<MyNetworkRoomPlayer>();
        if (playerCalledCmd?.PlayerColor == Color.clear) return;
        
        ServerChangeInLobbyState(conn);
        bool allReady = ServerCheckAllPlayersReady();
        if (allReady)
        {
            ServerStartBeginGameCounter();
        }
        else
        {
            if (startGameCounterCor != null)
            {
                StopCoroutine(startGameCounterCor);
                stopStartCounter = true;
                counterText = "Not all are ready";
            }
        }
    }

    public void ServerChangeInLobbyState(NetworkConnectionToClient conn)
    {
        if (!readyPlayers.ContainsKey(conn))
        {
            bool state = true;
            readyPlayers.Add(conn, state);
        }
        else
        {
            readyPlayers[conn] = !readyPlayers[conn];
        }
        
        MyNetworkRoomPlayer playerCalledCmd = NetworkServer.connections[conn.connectionId]?.identity.GetComponent<MyNetworkRoomPlayer>();
        playerCalledCmd.ServerSetReady(readyPlayers[conn]);
        TargetSetReadyButtonState(conn, readyPlayers[conn]);
    }

    [TargetRpc]
    private void TargetSetReadyButtonState(NetworkConnectionToClient conn, bool state)
    {
        readyButton.GetComponent<Image>().color = state ? Color.red : Color.white;
    }

    [Server]
    private bool ServerCheckAllPlayersReady()
    {
        foreach (var playerState in readyPlayers.Values)
        {
            if (playerState == false)
            {
                return false;
            }
        }

        return true;
    }

    [Server]
    private void ServerStartBeginGameCounter()
    {
        startGameCounterCor = StartCoroutine(StartCounter(5));
    }

    private IEnumerator StartCounter(int sec)
    {
        while (sec > 0)
        {
            yield return new WaitForSeconds(1);
            sec -= 1;
            counterText = sec.ToString();
        }
        MyNetworkRoomManager.Instance.LoadGameScene();
    }

    private void SyncCounter(string oldval, string newVal)
    {
        startGameCounter.text = newVal;
    }
}