using System.Collections.Generic;
using JamesFrowen.MirrorExamples;
using Mirror;
using UnityEngine;

public class Spawner : NetworkBehaviour
{
    public static Spawner Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Spawner>();
            }

            return instance;
        }
    }
    [SerializeField] public List<GameObject> SpawnPoints => spawnPoints;


    [SerializeField] private List<GameObject> spawnPoints;
    [SerializeField] private GameObject cannonPrefab;
    [SerializeField] private GameObject wallPrefab;
    [SerializeField] private PrefabPoolManager poolManager;
    [SerializeField] private Stack<Projectile> inactiveProjectiles = new Stack<Projectile>();
    [SerializeField] private Projectile projectilePrefab;
    private int spawnPointIndex;
    private static Spawner instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        MyNetworkRoomManager.Instance.OnPlayerDisconnect += ((conn) => { spawnPointIndex--; });
    }

    [Server]
    public void SpawnPlayerCannon(NetworkConnectionToClient conn)
    {
        MyInGamePlayer currentPlayer =  NetworkServer.connections[conn.connectionId].identity.GetComponent<MyInGamePlayer>();
        if (spawnPointIndex >= spawnPoints.Count) spawnPointIndex = 0;
        GameObject cannonObj = Instantiate(cannonPrefab, spawnPoints[spawnPointIndex].transform);
        NetworkServer.Spawn(cannonObj, conn);
        cannonObj.GetComponent<SyncParent>().SetParent(spawnPointIndex);
        currentPlayer.PlayerCanvas = cannonObj.GetComponent<PlayerWorldCanvas>();
        
        Color playerColor = NetworkServer.connections[conn.connectionId].identity.GetComponent<MyInGamePlayer>().Color;
        cannonObj.GetComponent<Cannon>().SetCannonColor(playerColor);
        currentPlayer.PlayerCanvas.Name = currentPlayer.Name;
        currentPlayer.PlayerCanvas.Score = currentPlayer.Score;
        Transform wallPointToSpawn = cannonObj.GetComponent<Cannon>().GetWallPoint();
        GameObject wallObj = Instantiate(wallPrefab, wallPointToSpawn.position,wallPointToSpawn.rotation);
        NetworkServer.Spawn(wallObj,conn);
        spawnPointIndex++;
    }


    private void AddInactiveProjectile(Projectile proj)
    {
        inactiveProjectiles.Push(proj);
    }

    public GameObject SpawnProjectile(Vector3 spawnPoint)
    {
        GameObject projectileToReturn;
        if (inactiveProjectiles.Count < 1)
        {
            projectileToReturn = Instantiate(projectilePrefab.gameObject, spawnPoint, Quaternion.identity);
            NetworkServer.Spawn(projectileToReturn);
            uint projectileId =
                projectileToReturn.GetComponent<NetworkIdentity>().netId;
            projectileToReturn.GetComponent<Projectile>().OnDeactivation += proj =>
            {
                inactiveProjectiles.Push(proj);
                RpcActivateObject(false, projectileId);
            };
        }
        else
        {
            projectileToReturn = inactiveProjectiles.Pop().gameObject;
            projectileToReturn.transform.position = spawnPoint;
            projectileToReturn.gameObject.SetActive(true);
            uint projectileId =
                projectileToReturn.GetComponent<NetworkIdentity>().netId;
            RpcActivateObject(true,projectileId);
        }
        
        return projectileToReturn;
    }

    [ClientRpc]
    private void RpcActivateObject(bool state,uint id)
    {
        NetworkClient.spawned[id].gameObject.SetActive(state);
    }
}