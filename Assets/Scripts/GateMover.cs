using Mirror;
using UnityEngine;

public class GateMover : NetworkBehaviour
{
    public GameObject gate;
    public GameObject wall;

    [SyncVar(hook = nameof(SyncGate))] [SerializeField]
    private Vector3 gatePos;

    public float speed = 25f;


    void Update()
    {
        if (isOwned)
        {
            int moveX = 0;
            int moveY = 0;

            if (Input.GetKey(KeyCode.W))
            {
                moveY = 1;
            }

            if (Input.GetKey(KeyCode.S))
            {
                moveY = -1;
            }

            if (Input.GetKey(KeyCode.A))
            {
                moveX = -1;
            }

            if (Input.GetKey(KeyCode.D))
            {
                moveX = 1;
            }

            if (moveX != 0 || moveY != 0)
            {
                CmdMoveGate(moveX, moveY);
            }
        }
    }

    [Command]
    private void CmdMoveGate(int moveZ, int moveY)
    {
        Vector3 newPosition = gate.transform.localPosition;
        
        if (moveZ > 0)
        {
            newPosition.z += speed * Time.deltaTime;
        }

        if (moveZ < 0)
        {
            newPosition.z -= speed * Time.deltaTime;
        }

        if (moveY > 0)
        {
            newPosition.y += speed * Time.deltaTime;
        }

        if (moveY < 0)
        {
            newPosition.y -= speed * Time.deltaTime;
        }
        
        Bounds wallBounds = wall.GetComponent<Collider>().bounds;
        Vector3 wallMinLocal = gate.transform.InverseTransformPoint(wallBounds.min);
        Vector3 wallMaxLocal = gate.transform.InverseTransformPoint(wallBounds.max);
        
        newPosition.z = Mathf.Clamp(newPosition.z, wallMinLocal.z, wallMaxLocal.z);
        newPosition.y = Mathf.Clamp(newPosition.y, wallMinLocal.y, wallMaxLocal.y);
        
        gate.transform.localPosition = newPosition;
        gatePos = newPosition;
    }

    private void SyncGate(Vector3 oldVal, Vector3 newVal)
    {
        gate.transform.localPosition = newVal;
    }
}