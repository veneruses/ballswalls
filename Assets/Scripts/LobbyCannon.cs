using Mirror;
using UnityEngine;

public class LobbyCannon : MonoBehaviour
{
    [SerializeField] private GameObject cannon;

    private void Start()
    {
        if (NetworkServer.active)
        {
            cannon.gameObject.SetActive(false);
        }
        else
        {
            ColorSelectionPanel.Instance.OnColorSelectionPanelChangeState += state =>
            {
                cannon.gameObject.SetActive(state);
            };
        }
    }

    public void SetCannonColor(Color color)
    {
        cannon.GetComponent<Renderer>().material.color = color;
    }
}
