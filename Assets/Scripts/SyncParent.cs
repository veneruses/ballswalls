using Mirror;
using UnityEngine;

public class SyncParent : NetworkBehaviour
{
   [SyncVar(hook = nameof(SyncObjectParent))] [SerializeField] private int spawnIndex=-100;
   
   private void SyncObjectParent(int oldVal, int newVal)
   {
      gameObject.transform.SetParent(Spawner.Instance.SpawnPoints[newVal].transform);
   }
   
   public void SetParent(int parentIndex)
   {
      spawnIndex = parentIndex;
   }
}
