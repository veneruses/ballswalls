using System;
using Mirror;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Action<Projectile> OnDeactivation;
    
    [SerializeField] private int playerConnectionId;
    [SerializeField] private bool hitAlready;
    [SerializeField] private float lifeTimeMax = 5;
    [SerializeField] private float lifeTime;
    
    public void SetPlayerId(int id)
    {
        playerConnectionId = id;
    }

    private void OnEnable()
    {
        lifeTime = lifeTimeMax;
    }

    private void Update()
    {
        if (NetworkServer.active)
        {
            lifeTime -= Time.deltaTime;
            if (lifeTime < 0)
            {
                Deactivate();
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!NetworkServer.active) return;
        
        if(hitAlready) return;
        if(!other.gameObject.CompareTag("Gate")) return;
        
        GateMover gates = other.gameObject.GetComponentInParent<GateMover>();
        if (gates != null)
        {
            MyInGamePlayer firedPlayer =
                NetworkServer.connections[playerConnectionId].identity.GetComponent<MyInGamePlayer>();
            firedPlayer.ChangePlayerScore(firedPlayer.Score + 1);
            int gatesPlayerId = gates.GetComponentInParent<NetworkIdentity>().connectionToClient.connectionId;
            MyInGamePlayer gatesPlayer =
                NetworkServer.connections[gatesPlayerId].identity.GetComponent<MyInGamePlayer>();
            gatesPlayer.ChangePlayerScore(gatesPlayer.Score - 1);
            hitAlready = true;
        }
    }

    private void Deactivate()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        hitAlready = false;
        OnDeactivation?.Invoke(this);
        gameObject.SetActive(false);
    }
}