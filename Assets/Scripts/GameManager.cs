using Mirror;

public class GameManager : NetworkBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameManager manager = FindObjectOfType<GameManager>();
                instance = manager;
                DontDestroyOnLoad(manager);
            }
            return instance;
        }
    }
    private NetworkBehaviour localPlayer;

    public NetworkBehaviour LocalPlayer
    {
        get { return localPlayer; }
        set { localPlayer = value; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (this != instance)
            {
                Destroy(this);
            }
        }
    }
    
    [TargetRpc]
    public void SetLocalPlayer(NetworkConnectionToClient conn, NetworkBehaviour player)
    {
        LocalPlayer = player;
    }
    
    
}
