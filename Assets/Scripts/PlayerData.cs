using UnityEngine;

public struct  PlayerData
{
    public string Name;
    public Color Color;
}
