using UnityEngine;

public class ResolutionSetter : MonoBehaviour
{
    private int defaultWidth = 1920;
    private int defaultHeight = 1080;

    void Start()
    {
        Screen.SetResolution(defaultWidth, defaultHeight, false);
    }
    
    
}